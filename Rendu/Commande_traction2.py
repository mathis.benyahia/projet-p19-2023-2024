# Programme permettant de commander le robot en détectant les erreurs de traction et en essayant de les corriger
import time
from spherov2 import scanner
import spherov2.controls.v2
from spherov2.sphero_edu import SpheroEduAPI
import threading
import numpy as np
from scipy.signal import butter, filtfilt
from scipy import integrate as intg
import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math
from spherov2.types import Color


def acceleration_progressive(droid, target_speed, initial_speed, time_action):
    delta = list(np.arange(0, time_action, 1))
    for i in delta:
        droid.roll2(int(np.floor(initial_speed + i *
                    (target_speed - initial_speed) / len(delta))), 1)
    print('la vitesse %s est atteinte.' % target_speed)
    return True

# on définit la fonction de calcul de jacobien (méthode des différences finies)


def jacobian(g, x, epsilon=1e-6):
    n = len(x)
    m = len(g(x))
    J = np.zeros((m, n))
    for i in range(n):
        x_plus = np.array(x, dtype=float)
        x_plus[i] += epsilon
        J[:, i] = (g(x_plus) - g(x)) / epsilon
    return J

# on définit spécifiquement la jacobienne de F (méthode des différences finies)


def jacobianF(x, epsilon=1e-6):
    n = 20
    J = np.zeros((n, n))
    # on ne dérive que par rapport aux variables de l'état X
    count = 0
    for i in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]:
        x_plus = np.array(x, dtype=float)
        x_plus[i] += epsilon
        J[:, count] = (f(x_plus)-f(x))/epsilon
        count += 1
    return J


# On définit g / on intègre aussi directement dans g le bruit lié au processus / ceci n'influence pas la linéarisation
G = np.zeros((7, 20))
G[0, 3] = 1
G[1, 4] = 1
G[2, 5] = 1
G[3, 6] = 1
G[4, 10] = 1
G[5, 11] = 1
G[6, 12] = 1
# Fonction g(x_k) qui applique la matrice de transformation à x_k


def g(x_k):
    return G @ x_k


def R_from_quaternion(q):
    qx = q[0]
    qy = q[1]
    qz = q[2]
    qw = q[3]

    R = np.array([
        [2*qx**2 + 2*qy**2 - 1, 2*qy*qz - 2*qx*qw, 2*qy*qw + 2*qx*qz],
        [2*qy*qz + 2*qx*qw, 2*qx**2 + 2*qz**2 - 1, 2*qz*qw - 2*qx*qy],
        [2*qy*qw - 2*qx*qz, 2*qz*qw - 2*qx*qy, 2*qx**2 + 2*qx - 1]
    ])

    return R


# on définit la fonction f de transition d'état / on intègre aussi dans f directement le bruti lié au processus / ceci n'influence pas l'étape de linéarisation
def f(x_u_k_1):
    p_k_1 = x_u_k_1[:3]
    q = x_u_k_1[3:7]
    p_prime_k_1 = x_u_k_1[7:10]
    p_seconde_mes = x_u_k_1[10:13]
    p_seconde_k_1 = x_u_k_1[13:16]
    bg_k_1 = x_u_k_1[16:20]
    ba_k_1 = x_u_k_1[20:23]
    q_mes = x_u_k_1[23:27]
    Delta_t = x_u_k_1[27]
    w_gyro = x_u_k_1[28:32]
    w_accel = x_u_k_1[32:35]

    g = np.array([0, 0, 9.81])

    p_new = p_k_1 + Delta_t * p_prime_k_1
    q_new = q_mes - bg_k_1
    R_k_1 = R_from_quaternion(q)
    p_prime_new = p_prime_k_1 + Delta_t * p_seconde_k_1
    p_double_prime_new = (R_k_1@((p_seconde_mes - ba_k_1).T)-g.T).T
    bg_new = bg_k_1 + w_gyro
    ba_new = ba_k_1 + w_accel

    x_new = np.hstack((p_new, q_new, p_prime_new,
                      p_double_prime_new, bg_new, ba_new))

    return x_new


P_k = np.eye(20)*0.1  # Matrice de covariance initiale
Q_k = np.eye(20)  # Bruit de processus
R_k = np.diag([0.0018574656909785153, 0.0007899863734917432, 0.002815868346258441,
              9.184047426577201e-07, 4.449800733609465e-05, 8.855133728300614e-05, 1.4073930791050955e-05])

# on applique le filtre de Kalman en temps réel


def kalman_filter_step(vecteur_etat_exp, time, y_mesure, P_k):
    """
    Fonction pour effectuer un pas de filtre de Kalman.

    Arguments:
    vecteur_etat_exp -- dict, vecteur d'état attendu à chaque instant de temps
    time -- array, temps discret
    y_mesure -- array, mesures à chaque instant de temps
    P_k -- array, matrice de covariance de l'état
    Q_k -- array, matrice de covariance du bruit de processus
    R_k -- array, matrice de covariance du bruit de mesure
    f -- function, modèle de prédiction de l'état
    jacobianF -- function, Jacobienne de la fonction f
    g -- function, modèle de prédiction de la mesure
    jacobian -- function, Jacobienne de la fonction g

    Retourne:
    vecteur_etat_exp -- dict, vecteur d'état attendu mis à jour
    P_k -- array, matrice de covariance de l'état mise à jour
    """
    for index, step in enumerate(time[:-1]):
        # Valeur mesurée à l'instant step
        u = y_mesure[step]
        w_gyro = np.random.normal(0, 1/2, 4)
        w_accel = np.random.normal(0, 1/2, 3)

        if index == 0:
            x = np.hstack((vecteur_etat_exp[step][0:3], vecteur_etat_exp[step][3:7], vecteur_etat_exp[step][7:10],
                           u[0:3], vecteur_etat_exp[step][10:14], vecteur_etat_exp[step][14:17],
                           vecteur_etat_exp[step][17:], u[3:], [step], w_gyro, w_accel))
        else:
            x = np.hstack((vecteur_etat_exp[step][0:3], vecteur_etat_exp[step][3:7], vecteur_etat_exp[step][7:10],
                           u[0:3], vecteur_etat_exp[step][10:14], vecteur_etat_exp[step][14:17],
                           vecteur_etat_exp[step][17:], u[3:], [time[index+1]-step], w_gyro, w_accel))

        x_k_minus = f(x)
        x = np.hstack((x_k_minus[0:3], x_k_minus[3:7], x_k_minus[7:10], u[0:3], x_k_minus[10:14],
                       x_k_minus[14:17], x_k_minus[17:], u[3:], [time[index+1]-step], w_gyro, w_accel))

        F_k = jacobianF(x)

        # Phase de prédiction
        P_k_minus = F_k @ P_k @ F_k.T + Q_k

        # Phase de mise à jour
        H_k = jacobian(g, x_k_minus)
        K_k = P_k_minus @ H_k.T @ np.linalg.inv(H_k @ P_k_minus @ H_k.T + R_k)
        x_k = x_k_minus + K_k @ (u - g(x_k_minus))
        P_k = (np.eye(len(x_k)) - K_k @ H_k) @ P_k_minus

        vecteur_etat_exp[time[index+1]] = x_k

    return vecteur_etat_exp


# calcul de corrélation entre les signaux
def coefficient_pearson(signal1, signal2):
    # Convertir les signaux en tableaux NumPy
    signal1 = np.array(signal1)
    signal2 = np.array(signal2)

    # Calculer les moyennes des signaux
    mean1 = np.mean(signal1)
    mean2 = np.mean(signal2)

    # Calculer les termes numérateur et dénominateur
    num = np.sum((signal1 - mean1) * (signal2 - mean2))
    den = np.sqrt(np.sum((signal1 - mean1) ** 2)
                  * np.sum((signal2 - mean2) ** 2))

    # Calculer le coefficient de corrélation de Pearson
    if den == 0:
        return 0
    else:
        return num / den


def detection_glissement(droid):
    start_time = time.time()
    donnees = {}
    donnes_usefull = {}
    vitesse_test = {}
    while time.time() - start_time < 1:
        accelerometer_data = droid.get_acceleration()
        velocity_data = droid.get_velocity()
        quaternion = droid.get_quaternion()
        if accelerometer_data is not None:
            accelerometer_values = [accelerometer_data.get(
                'x', 0), accelerometer_data.get('y', 0), accelerometer_data.get('z', 0)]
            roll_values = [velocity_data['x'], velocity_data['y']]
            temps_actuel = time.time() - start_time
            data_actuel = accelerometer_values + \
                [quaternion.get('x', 0), quaternion.get('y', 0),
                 quaternion.get('z', 0), quaternion.get('w', 0)]
            if donnees:
                previous_temps = max(donnees.keys())
                if donnees[previous_temps] != data_actuel:
                    donnees[temps_actuel] = data_actuel
                    donnes_usefull[temps_actuel] = data_actuel
                    vitesse_test[temps_actuel] = roll_values
            else:
                donnees[temps_actuel] = data_actuel
    vecteur_etat_exp = {list(donnes_usefull.keys())[0]: np.zeros(20)}
    Kalman = kalman_filter_step(
        vecteur_etat_exp, list(donnes_usefull.keys()), donnes_usefull, P_k)
    filtered_velocity = [Kalman[tempsexp][6]
                         for tempsexp in list(Kalman.keys())]
    theorical_velocity = [0]+[vitesse_test[tempsexp2][1]
                              for tempsexp2 in list(vitesse_test.keys())][1:]
    print(coefficient_pearson(filtered_velocity, theorical_velocity))
    print(filtered_velocity)
    print(theorical_velocity)
    if coefficient_pearson(filtered_velocity, theorical_velocity) < 0.7:
        return True
    else:
        return False


def avancer_en_ligne_droite(droid, vitesse, duree_avancee):
    droid.roll(duration=2, speed=0, heading=0)
    droid.roll(duration=duree_avancee, speed=vitesse, heading=0)
    droid.roll(duration=2, speed=0, heading=0)


def boucle_traction(droid, start_time, duree):
    acceleration_progressive(
        droid, target_speed=40, initial_speed=0, time_action=5)
    if time.time()-start_time > duree:  # si la durée de simulation est dépassé
        droid.stop_roll()
    if detection_glissement(droid):  # si un glissement est détecté
        print('Glissement')
        droid.stop_roll()  # on arrete le robot
        boucle_traction(droid, start_time-time.time(),
                        duree)  # on relance la boucle
    else:
        boucle_traction(droid, start_time-time.time(),
                        duree)  # on relance la boucle


# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot_2)
print(toy)

with SpheroEduAPI(toy) as droid:
    start_t = time.time()
    duree_totale = 30  # durée de simulation
    angle_objectif = 0  # direction à viser
    # Initialisation de la boussole du robot

    # Mise en place de thread pour utiliser les 2 boules en parallèles
    # Boucle Traction
    thread_traction = threading.Thread(
        target=boucle_traction, args=(droid, start_t, duree_totale))
    thread_traction.start()
    thread_traction.join()
