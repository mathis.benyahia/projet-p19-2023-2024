import numpy as np
import matplotlib.pyplot as plt
def EKF(X,DATA):
    #vitesse dans axes mobiles
    u = X[0]
    v = X[1]
    w = X[2]
    V = [u,v,w]
    #position objet
    x=X[3]
    y=X[4]
    z=X[5]
    XI = [x,y,z]
    #quaternion
    q0=X[6]
    q1=X[7]
    q2=X[8]
    q3=X[9]
    Q = [q0,q1,q2,q3]
    #vecteur biais gyroscope
    vp=X[10]
    vq=X[11]
    vr=X[12]
    nu = [vp,vq,vr]
    #vecteur biais accelerometre
    mux=X[13]
    muy=X[14]
    muz=X[15]
    mu = [mux,muy,muz]
    #vecteur d'etat
    X = np.array(V+XI+Q+nu+mu)
    C = np.array([[q0**2+q1**2+q2**3+q3**2,2*(q1*q2-q0*q3),2*(q0*q2+q1*q3)],[2*(q1*q2+q0*q3),q0**2-q1**2+q2**2-q3**2,2*(q2*q3+q0*q1)],[2*(q1*q3-q0*q2),2*(q0*q1+q2*q3),q0**2-q1**2-q2**2+q3**2]])

    #correction des vitesses angulaires :
    wx = DATA[0]
    wy = DATA[1]
    wz = DATA[2]
    gyro = np.array([wx,wy,wz])
    nu_terrestre = C*nu
    Corr_vit_ang = np.array([[(gyro[1]-nu[1])*V[2]-(gyro[2]-nu[2])*V[1]],[(gyro[2]-nu[2])*V[0]-(gyro[0]-nu[0])*V[2]],[(gyro[0]-nu[0])*V[1]-(gyro[1]-nu[1])*V[0]]])

    #derivé de la vitesse dans le referentiel mobile --> acceleration referentiel mobile
    ax = DATA[3]
    ay = DATA[4]
    az = DATA[5]
    accelerometre = np.array([[ax],[ay],[az]])
    g = np.array([[0],[0],[9.81]])
    V_prime = accelerometre+C.T@g-Corr_vit_ang
    print(Corr_vit_ang)
    #dérivé vecteur position
    XI_prime = C@V
    XI_prime = XI_prime.reshape(-1,1)
    #dérivé des quaternions
    delta_T = 1/10
    k_lambda = (1/delta_T)*(1-(q0**2+q1**2+q2**2+q3**3))
    # Matrice 4x3
    A = 0.5 * np.array([[-q1, -q2, -q3],[q0, -q3, q2],[q3, q0, -q1],[-q2, q1, q0]])

    B = np.array([[gyro[0] - nu[0]],[gyro[1] - nu[1]],[gyro[2] - nu[2]]])
    C = A @ B

    # Matrice Q doit être de dimensions (4, 1) pour que l'addition soit correcte
    Q = np.array(Q)
    Q = Q.reshape((4, 1))

    # Addition de k_lambda * Q
    Q_prime = C + k_lambda * Q
    #bruit blanc gaussien et marche aléatoire
    n = 3
    white_noise = np.random.normal(loc=0, scale=1, size=n)
    random_steps = np.random.normal(loc=0, scale=1, size=n)
    random_walk = np.cumsum(random_steps)

    #derivé des bruits
    tau_x = tau_y = tau_z = 1
    nu_prime = white_noise.reshape(-1,1)
    mu_prime = (np.diag([1/tau_x,1/tau_y,1/tau_z])@mu + white_noise).reshape(-1,1)
    #dérivé générale
    V_prime_T = V_prime.T
    XI_prime_T = XI_prime.T
    Q_prime_T = Q_prime.T
    nu_prime_T = nu_prime.T
    mu_prime_T = mu_prime.T
    X_prime = np.hstack((V_prime_T, XI_prime_T, Q_prime_T, nu_prime_T, mu_prime_T))
    freq = 30
    Xcorr = X + X_prime[0]/freq
    return Xcorr