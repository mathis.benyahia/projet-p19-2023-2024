import numpy as np
import matplotlib.pyplot as plt
import csv
import numpy as np
from filterpy.kalman import ExtendedKalmanFilter as EKF
from filterpy.common import Q_discrete_white_noise
from scipy.spatial.transform import Rotation as R
from scipy.signal import butter, filtfilt
import math

time = []
acceleration_x = []
acceleration_y = []
acceleration_z = []
gyro_x = []
gyro_y = []
gyro_z = []
theoric_velocity_x = []
theoric_velocity_y = []
theoric_velocity_z = []
quaternion_x = []
quaternion_y = []
quaternion_z = []
quaternion_w = []

with open('data/test_encodeur_robot.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        gyro_x.append(float(row[1]))
        gyro_y.append(float(row[2]))
        gyro_z.append(float(row[3]))
        acceleration_x.append(float(row[4]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity_x.append(float(row[7]))
        theoric_velocity_y.append(float(row[8]))
        theoric_velocity_z.append(float(0))
        quaternion_x.append(float(row[9]))
        quaternion_y.append(float(row[10]))
        quaternion_z.append(float(row[11]))
        quaternion_w.append(float(row[12]))


first_acceleration_x = acceleration_x[2]
first_acceleration_y = acceleration_y[2]
first_acceleration_z = acceleration_z[2]

# Soustraction de la première valeur à chaque valeur dans les listes

#acceleration_x = [ax - first_acceleration_x for ax in acceleration_x]
#acceleration_y = [ay - first_acceleration_y for ay in acceleration_y]
#acceleration_z = [az - first_acceleration_z for az in acceleration_z]


# Définition de la fréquence d'échantillonnage et de la fréquence de coupure du filtre passe-bas
fs = 15.0  # Fréquence d'échantillonnage (Hz)
cutoff_frequency = 1.5  # Fréquence de coupure du filtre passe-bas (Hz)

# Fonction pour appliquer un filtre passe-bas
def apply_lowpass_filter(data, fs, cutoff):
    nyquist_frequency = 0.5 * fs
    normalized_cutoff = cutoff / nyquist_frequency
    b, a = butter(1, normalized_cutoff, btype='low', analog=False)
    filtered_data = filtfilt(b, a, data)
    return filtered_data

# Appliquer le filtre passe-bas aux données d'accélération et de vitesse angulaire
acceleration_x = apply_lowpass_filter(acceleration_x, fs, cutoff_frequency)
acceleration_y = apply_lowpass_filter(acceleration_y, fs, cutoff_frequency)
acceleration_z = apply_lowpass_filter(acceleration_z, fs, cutoff_frequency)
#gyro_x = apply_lowpass_filter(gyro_x, fs, cutoff_frequency)
#gyro_y = apply_lowpass_filter(gyro_y, fs, cutoff_frequency)
#gyro_z = apply_lowpass_filter(gyro_z, fs, cutoff_frequency)
#acceleration_x = acceleration_x - acceleration_x[0]
#acceleration_y = acceleration_y - acceleration_y[0]
acceleration_z = acceleration_z - acceleration_z[0]

# Fonction pour transformer les quaternions en matrices de rotation
def quaternion_to_rotation_matrix(qx, qy, qz, qw):
    r = R.from_quat([qx, qy, qz, qw])
    return r.as_matrix()

# Définition de la Jacobienne de la fonction de mesure
def HJacobian(x):
    H = np.zeros((3, 9))
    H[0, 3] = 1
    H[1, 4] = 1
    H[2, 5] = 1
    return H

# Définition de la fonction de mesure
def Hx(x):
    return x[3:6]

# Définition de l'EKF
class RobotEKF(EKF):
    def __init__(self, dt):
        super().__init__(dim_x=9, dim_z=3)
        self.dt = dt
        self.F = np.eye(9)
        self.F[0, 3] = self.dt
        self.F[1, 4] = self.dt
        self.F[2, 5] = self.dt
        self.H = np.zeros((3, 9))
        self.H[0, 3] = 1
        self.H[1, 4] = 1
        self.H[2, 5] = 1
        self.Q = Q_discrete_white_noise(dim=3, dt=self.dt, var=0.1, block_size=3)
        self.R = np.eye(3)

    def predict(self, u):
        rotation_matrix = quaternion_to_rotation_matrix(u[0], u[1], u[2], u[3])
        acceleration_world = np.dot(rotation_matrix, u[4:])
        self.x[3:6] += acceleration_world * self.dt
        self.x[0:3] += self.x[3:6] * self.dt
        super().predict()

    def update(self, z):
        super().update(z, HJacobian, Hx)

immobile_start_index=0
immobile_end_index=20
dt = np.mean(np.diff(time))
# Calcul de la moyenne et de la variance de l'accélération pendant la période immobile
mean_acc_x = np.mean(acceleration_x[immobile_start_index:immobile_end_index])
mean_acc_y = np.mean(acceleration_y[immobile_start_index:immobile_end_index])
mean_acc_z = np.mean(acceleration_z[immobile_start_index:immobile_end_index])

var_acc_x = np.var(acceleration_x[immobile_start_index:immobile_end_index])
var_acc_y = np.var(acceleration_y[immobile_start_index:immobile_end_index])
var_acc_z = np.var(acceleration_z[immobile_start_index:immobile_end_index])

var_quat_x = np.var(quaternion_x[immobile_start_index:immobile_end_index])
var_quat_y = np.var(quaternion_y[immobile_start_index:immobile_end_index])
var_quat_z = np.var(acceleration_z[immobile_start_index:immobile_end_index])
var_quat_w = np.var(acceleration_x[immobile_start_index:immobile_end_index])


# Initialisation du filtre de Kalman étendu
ekf = RobotEKF(dt=dt)
ekf.x = np.zeros(9)  # [pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, acc_x, acc_y]
ekf.P *= 10  # Matrice de covariance de l'état initial

# Utilisation de la variance de l'accélération pour initialiser le bruit de processus
Q_var_acc = np.diag([var_acc_x, var_acc_y, var_acc_z])
Q = np.block([[np.zeros((3, 9))],
              [np.zeros((3, 3)), np.zeros((3, 6))],
              [np.zeros((3, 6)), Q_var_acc]])
ekf.Q = Q

# Estimation du bruit de mesure en utilisant la variance des différences entre les mesures et la moyenne de l'accélération
R_x = np.var(np.array(acceleration_x[immobile_start_index:immobile_end_index]) - mean_acc_x)
R_y = np.var(np.array(acceleration_y[immobile_start_index:immobile_end_index]) - mean_acc_y)
R_z = np.var(np.array(acceleration_z[immobile_start_index:immobile_end_index]) - mean_acc_z)
ekf.R = np.diag([R_x, R_y, R_z])

# Traitement des données
positions=[]
velocities=[]
for i in range(len(time)):
    print(i)
    u = [quaternion_x[i], quaternion_y[i], quaternion_z[i], quaternion_w[i], acceleration_x[i], acceleration_y[i], acceleration_z[i]]
    ekf.predict(u)
    #ekf.update([0, 0, 0])
    positions.append(ekf.x[:3].tolist())
    velocities.append(ekf.x[3:6].tolist())


def projeter_vitesses(vitesses_capteur, quat_x, quat_y, quat_z, quat_w):
    vitesses_terrestres = []
    for i in range(len(quat_x)):
        q = [quat_x[i], quat_y[i], quat_z[i], quat_w[i]]
        r = R.from_quat(q)
        vitesse_capteur = vitesses_capteur[i]
        vitesse_terrestre = r.apply(vitesse_capteur)
        vitesses_terrestres.append(vitesse_terrestre)
    return vitesses_terrestres

# Plotting
velocity_x = [velocities[i][0] for i in range(len(velocities))]
velocity_y = [velocities[i][1] for i in range(len(velocities))]
velocity_z = [velocities[i][2] for i in range(len(velocities))]


#plt.plot(time,np.array(velocity_x), label='Filtered velocity x')
#plt.plot(time, np.array(velocity_y), label='Filtered velocity y')
#plt.plot(time, np.array(velocity_z), label='Filtered velocity z')
plt.plot(time, apply_lowpass_filter(theoric_velocity_y,fs,cutoff_frequency), label='Theorical velocity')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Theorical velocity given by the robot')
plt.legend()
plt.grid(True)
plt.show()
