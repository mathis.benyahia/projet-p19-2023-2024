# Premier programme pour essayer de commander le robot en détectant les erreurs de cap
import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math


# Fonction pour surveiller l'angle de lacet et réagir en conséquence

def avancer_en_ligne_droite(droid, vitesse, duree_avancee):
    droid.set_stabilization(True)

    droid.roll(duration=duree_avancee, speed=vitesse, heading=0)


def surveiller_angle_lacet(droid, duree, angle_objectif):
    droid.set_stabilization(True)
    while time.time() - start_time < duree:

        orientation_values = droid.get_orientation()
        if orientation_values is not None:
            current_yaw = orientation_values.get('yaw', 0)
            print(current_yaw)
            angle_difference = angle_objectif - current_yaw
            # angle_objectif = angle_objectif - current_yaw
            '''if abs(angle_difference) > 5:
                droid.stop_roll()'''
            droid.roll(duration=1, speed=50, heading=angle_objectif)
            time.sleep(0.1)  # Attente avant la prochaine vérification
    droid.set_stabilization(False)


# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot)
print(toy)
# definition des variables
vitesse = 80

duree_avancee = 10
duree_totale = 20
intervalle_releve_accelerometre = 0.15

# Connexion au robot
with SpheroEduAPI(toy) as droid:
    # Nom du fichier CSV pour enregistrer les données de l'accéléromètre
    '''fichier_csv = "data/donnees_accelerometre_V2_2.csv"

    # Création du thread pour faire avancer le robot en ligne droite
    thread_avancer = threading.Thread(
        target=avancer_en_ligne_droite, args=(droid, vitesse, duree_avancee))
    thread_avancer.start()  # Démarrage du thread pour faire avancer le robot
    # Création du thread pour surveiller l'angle de lacet

    thread_angle = threading.Thread(
        target=surveiller_angle_lacet, args=(droid, angle_objectif))
    thread_angle.start()

    # avancer_en_ligne_droite(droid, vitesse, duree_avancee)
    # Attendre que le thread d'avancement du robot se termine
    # thread_avancer.join()
    thread_angle.join()
    '''
    start_time = time.time()
    angle_objectif = 0
    surveiller_angle_lacet(droid, duree_totale, angle_objectif)
    print('threads terminés')
