import numpy as np
import matplotlib.pyplot as plt
import csv
from filterpy.kalman import UnscentedKalmanFilter, MerweScaledSigmaPoints
from filterpy.common import Q_discrete_white_noise
from scipy.spatial.transform import Rotation as R
from scipy.signal import butter, filtfilt
from ExtendedKalmanFilter import ExtendedKalmanFilter
from scipy.integrate import odeint

time = []
acceleration_x = []
acceleration_y = []
acceleration_z = []
gyro_x = []
gyro_y = []
gyro_z = []
theoric_velocity_x = []
theoric_velocity_y = []
theoric_velocity_z = []
quaternion_x = []
quaternion_y = []
quaternion_z = []
quaternion_w = []

with open('data/donnees_accelerometer_V5_15Hz.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        gyro_x.append(float(row[1]))
        gyro_y.append(float(row[2]))
        gyro_z.append(float(row[3]))
        acceleration_x.append(float(row[4]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity_x.append(float(row[7]))
        theoric_velocity_y.append(float(row[8]))
        theoric_velocity_z.append(float(0))
        quaternion_x.append(float(row[9]))
        quaternion_y.append(float(row[10]))
        quaternion_z.append(float(row[11]))
        quaternion_w.append(float(row[12]))


first_acceleration_x = acceleration_x[5]
first_acceleration_y = acceleration_y[5]
first_acceleration_z = acceleration_z[5]

# Définition de la fréquence d'échantillonnage et de la fréquence de coupure du filtre passe-bas
fs = 15.0  # Fréquence d'échantillonnage (Hz)
cutoff_frequency = 1.0  # Fréquence de coupure du filtre passe-bas (Hz)

# Fonction pour appliquer un filtre passe-bas
def apply_lowpass_filter(data, fs, cutoff):
    nyquist_frequency = 0.5 * fs
    normalized_cutoff = cutoff / nyquist_frequency
    b, a = butter(1, normalized_cutoff, btype='low', analog=False)
    filtered_data = filtfilt(b, a, data)
    return filtered_data

# Appliquer le filtre passe-bas aux données d'accélération et de vitesse angulaire
acceleration_x = apply_lowpass_filter(acceleration_x, fs, cutoff_frequency)
acceleration_y = apply_lowpass_filter(acceleration_y, fs, cutoff_frequency)
acceleration_z = apply_lowpass_filter(acceleration_z, fs, cutoff_frequency)
gyro_x = apply_lowpass_filter(gyro_x, fs, cutoff_frequency)
gyro_y = apply_lowpass_filter(gyro_y, fs, cutoff_frequency)
gyro_z = apply_lowpass_filter(gyro_z, fs, cutoff_frequency)

#EKF
deltaTime=np.mean(np.diff(time))
x0 = np.zeros(1,10)
SimulationSteps = len(time)
TotalSimulationTime = time[-1]

def StateSpaceModel(x,t):

# Plotting
'''
plt.plot(time, np.array(velocities), label='Filtered velocity')
plt.plot(time, theoric_velocity_y, label='Experimental velocity')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Velocity vs Time')
plt.legend()
plt.grid(True)
plt.show()
'''