import time
import csv as csv
from spherov2 import scanner
import spherov2.toy.bolt as boltclass
from spherov2.sphero_edu import SpheroEduAPI
import threading
import spherov2
import queue
import recuperation_donnees
import threading
import time
import csv

# Créer un verrou pour synchroniser l'accès aux ressources partagées
lock = threading.Lock()

# Fonction pour avancer le robot en ligne droite


def avancer_en_ligne_droite(toy):
    with SpheroEduAPI(toy) as droid:
        time.sleep(2)
        droid.roll(0, 50, 5)
        droid.roll(0, 80, 3)
        time.sleep(2)

# Fonction pour enregistrer les données de l'accéléromètre pendant que le robot avance


def enregistrer_accelerometre_pendant_avance(toy, duree, fichier_csv):
    with open(fichier_csv, mode='a', newline='') as file:
        writer = csv.writer(file)
        start_time = time.time()
        donnees = {}
        while time.time() - start_time < duree:
            # with lock:  # Acquérir le verrou
            accelerometer_data = droid.get_acceleration()
            gyro_data = droid.get_gyroscope()
            velocity_values = droid.get_velocity()
            quaternion_values = droid.get_quaternion()
            if accelerometer_data or gyro_data is not None:
                accelerometer_values = [accelerometer_data.get(
                    'x', 0), accelerometer_data.get('y', 0), accelerometer_data.get('z', 0)]
                gyro_values = [gyro_data.get('x', 0), gyro_data.get(
                    'y', 0), gyro_data.get('z', 0)]
                quaternion_values = [quaternion_values.get('x', 0), quaternion_values.get(
                    'y', 0), quaternion_values.get('z', 0), quaternion_values.get('w', 0)]
                roll_values = [velocity_values['x'], velocity_values['y']]
                temps_actuel = time.time()-start_time
                data_actuel = gyro_values + accelerometer_values + roll_values + quaternion_values
                if donnees:
                    previous_temps = max(donnees.keys())
                    if donnees[previous_temps] != data_actuel:
                        donnees[temps_actuel] = data_actuel
                        print(data_actuel)
                        writer.writerow([temps_actuel]+data_actuel)
                else:
                    donnees[temps_actuel] = data_actuel
                    writer.writerow([temps_actuel] + data_actuel)
            else:
                print("Aucune donnée d'accélération disponible")


# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot_2)
print(toy)

fichier_csv = "data/test_lock.csv"
with SpheroEduAPI(toy) as droid:
    scanner.BOLT.set_sensor_streaming_mask(toy, 150, 0, 63431290)
    print(scanner.BOLT.get_sensor_streaming_mask(toy))

# Connexion au robot

    # Nom du fichier CSV pour enregistrer les données de l'accéléromètre

thread_data = threading.Thread(
    target=enregistrer_accelerometre_pendant_avance, args=(toy, 15, fichier_csv))
thread_avancer = threading.Thread(target=avancer_en_ligne_droite, args=(toy,))
thread_data.start()
thread_avancer.start()


print('thread terminé')
