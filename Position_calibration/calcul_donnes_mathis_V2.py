import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy.signal import butter, filtfilt
from scipy import integrate as intg


def velocity_from_acceleration(time, acceleration):
    dt = np.diff(time)  # Calculate time intervals
    velocity = intg.cumulative_trapezoid(acceleration,time, initial=0)
    return velocity


# Load acceleration data from CSV file
time = []
acceleration_y = []
acceleration_z = []
alpha = []
theoric_velocity = []

with open('data/donnees_accelerometre_V2_2.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity.append(float(row[7]))
        alpha.append(float(row[1]))

# filtrage des données (passe bas pour l'accelerometre et passe haut pour le gyroscope)
"""
Args:
- data : Signal à filtrer.
- cutoff : Fréquence de coupure du filtre en Hz.
- fs : Fréquence d'échantillonnage du signal en Hz.
- order : Ordre du filtre, par défaut 5.
"""
ecarts = [time[i+1] - time[i] for i in range(len(time)-1)]
moyenne_ecart = sum(ecarts) / len(ecarts)
fs = 10
fc = 0.235
w = fc / (fs / 2)

b, a = butter(5, w, 'low')
acceleration_z_filtered = filtfilt(b,a,acceleration_z)
acceleration_y_filtered = filtfilt(b,a,acceleration_y)

# mise en forme des données

acceleration_z_filtered_form = (np.array(acceleration_z)-acceleration_z[0])*9.8
acceleration_y_filtered_form = (np.array(acceleration_y))*9.8
alpha = np.array(alpha)
time = np.array(time)



# Calculate velocity

velocity_y = velocity_from_acceleration(time,acceleration_y_filtered_form)
velocity_z = velocity_from_acceleration(time,acceleration_z_filtered_form)

projection_velocity_y = (np.cos(alpha*np.pi/180))*(-(velocity_y))

#plt.plot(time, acceleration_y_filtered, label=' filtered acceleration')
#plt.plot(time, real_acceleration_z, label=' filtered acceleration z')
plt.plot(time,projection_velocity_y, label=' filtered velocity')
plt.plot(time, theoric_velocity, label=' experimental velocity')
#plt.plot(time,-200*velocity_from_acceleration(time,acceleration_y),label='root velocity')
# plt.plot(time,speed,label='velocity from wheels')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Velocity vs Time')
plt.legend()
plt.grid(True)
plt.show()
