# Programme permettant de commander le robot en détectant à la fois les erreurs de glissement et de cap
# et qui essaye de les corriger.
import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math
from spherov2.types import Color

from Commande_traction2 import detection_glissement, acceleration_progressive
from commande_V3 import surveiller_angle_lacet, set_angle

# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot_2)
print(toy)

# Fonction permettant de gérer la traction


def boucle_traction(droid, start_time, duree):
    acceleration_progressive(
        droid, target_speed=40, initial_speed=0, time_action=5)
    if time.time()-start_time > duree:  # si la durée de simulation est dépassé
        droid.stop_roll()
    if detection_glissement(droid):  # si un glissement est détecté
        print('Glissement')
        droid.stop_roll()  # on arrete le robot
        boucle_traction(droid, start_time, duree)  # on relance la boucle


# Connexion au robot
with SpheroEduAPI(toy) as droid:
    start_t = time.time()
    duree_totale = 30  # durée de simulation
    angle_objectif = 0  # direction à viser
    # Initialisation de la boussole du robot
    set_angle(droid)

    # Mise en place de thread pour utiliser les 2 boules en parallèles
    # Boucle Traction
    thread_traction = threading.Thread(
        target=boucle_traction, args=(droid, start_t, duree_totale))
    thread_traction.start()

    # Boucle Cap
    thread_rotation = threading.Thread(
        target=surveiller_angle_lacet, args=(droid, duree_totale, 0, start_t))
    thread_rotation.start()

    thread_traction.join()
    thread_rotation.join()

print('Fin')
