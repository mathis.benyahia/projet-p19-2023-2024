import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy.signal import butter, filtfilt
from scipy import integrate as intg
from scipy.spatial.transform import Rotation
from numpy.linalg import norm
from filterpy.kalman import ExtendedKalmanFilter


def velocity_from_acceleration(time, acceleration):
    velocity = intg.cumulative_simpson(y=acceleration, x=time)
    return velocity


# Load acceleration data from CSV file
time = []
acceleration_x = []
acceleration_y = []
acceleration_z = []
rot_x = []
rot_y = []
rot_z = []
theoric_velocity = []
quat_x = []
quat_y = []
quat_z = []
quat_w = []

with open('Position_calibration/data/donnees_accelerometer_V3_7.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        rot_x.append(float(row[1]))
        rot_y.append(float(row[2]))
        rot_z.append(float(row[3]))
        acceleration_x.append(float(row[4]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity.append(float(row[7]))
        quat_x.append(float(row[8]))
        quat_y.append(float(row[9]))
        quat_z.append(float(row[10]))
        quat_w.append(float(row[11]))

# Define the Extended Kalman Filter
ekf = ExtendedKalmanFilter(dim_x=6, dim_z=3)

# Define state transition function
# Define state transition function


def state_transition_function(x, dt):
    # State transition function for a system composed of gyroscope and accelerometer
    # Here, x represents the state vector which could include angles and angular velocities
    # dt is the time step

    # Assuming x[0], x[1], x[2] represent the angles (roll, pitch, yaw) and
    # x[3], x[4], x[5] represent the angular velocities

    # Integrate angular velocities to update angles
    x[0] += x[3] * dt  # roll angle update
    x[1] += x[4] * dt  # pitch angle update
    x[2] += x[5] * dt  # yaw angle update

    return x

# Define measurement function


def measurement_function(x):
    # Measurement function for a system composed of gyroscope and accelerometer
    # Here, x represents the state vector

    # Assuming measurements consist of acceleration along x, y, and z axes
    # These could be extracted from accelerometer readings

    # Compute gravity vector in body frame based on current angles
    gravity_body = np.array([
        np.sin(x[1]),  # x[1] is pitch angle
        # x[0] is roll angle, x[1] is pitch angle
        -np.sin(x[0]) * np.cos(x[1]),
        -np.cos(x[0]) * np.cos(x[1])  # x[0] is roll angle, x[1] is pitch angle
    ])

    # Rotate gravity vector to global frame using current angles
    rotation_matrix = Rotation.from_euler(
        'xyz', x[:3], degrees=False).as_matrix()
    gravity_global = np.dot(rotation_matrix, gravity_body)

    # Return acceleration measurements (gravity compensated)
    return gravity_global


# Initialize the filter
ekf.x = np.zeros(6)  # Initial state guess
ekf.P *= 0.01  # Initial covariance guess
ekf.Q *= 0.01  # Process noise covariance
ekf.R *= 0.01  # Measurement noise covariance

# Run the filter over the data
filtered_state = []
for i in range(len(time)):
    # Prediction step
    ekf.predict()

    # Update step with measurement
    measurement = np.array(
        [acceleration_x[i], acceleration_y[i], acceleration_z[i]])
    ekf.update(z=measurement, HJacobian=state_transition_function,
               Hx=measurement_function)

    # Save the filtered state
    filtered_state.append(ekf.x)
