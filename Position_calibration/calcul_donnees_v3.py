import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy.signal import butter
from scipy import integrate as intg
from scipy.spatial.transform import Rotation
from numpy.linalg import norm


def velocity_from_acceleration(time, acceleration):
    velocity = intg.cumulative_simpson(y=acceleration, x=time)
    return velocity


def integrate_gyroscope(dt, gyroscope, m_local_to_world_quat):
    av = gyroscope * dt * 0.5
    l = np.linalg.norm(av)
    s = np.sin(l) / l
    hd = av * s
    hd_w = np.cos(l)

    a = m_local_to_world_quat
    w = -hd[0] * a[0] - hd[1] * a[1] - hd[2] * a[2]
    x = hd[0] * a[3] + hd_w * a[0] + hd[2] * a[1] - hd[1] * a[2]
    y = hd[1] * a[3] + hd_w * a[1] + hd[0] * a[2] - hd[2] * a[0]
    z = hd[2] * a[3] + hd_w * a[2] + hd[1] * a[0] - hd[0] * a[1]

    m_local_to_world_quat[0] += x
    m_local_to_world_quat[1] += y
    m_local_to_world_quat[2] += z
    m_local_to_world_quat[3] += w
    m_local_to_world_quat = m_local_to_world_quat / np.linalg.norm(m_local_to_world_quat)



# Load acceleration data from CSV file
time = []
acceleration_x = []
acceleration_y = []
acceleration_z = []
gyro_x = []
gyro_y = []
gyro_z = []
theoric_velocity_x = []
theoric_velocity_y = []
theoric_velocity_z = []
quaternion_x = []
quaternion_y = []
quaternion_z = []
quaternion_w = []

with open('data/test_encodeur_robot.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        gyro_x.append(float(row[1]))
        gyro_y.append(float(row[2]))
        gyro_z.append(float(row[3]))
        acceleration_x.append(float(row[4]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity_x.append(float(row[7]))
        theoric_velocity_y.append(float(row[8]))
        theoric_velocity_z.append(float(0))
        quaternion_x.append(float(row[9]))
        quaternion_y.append(float(row[10]))
        quaternion_z.append(float(row[11]))
        quaternion_w.append(float(row[12]))

# Filtrage des données
fs = 15
fc = 1
w = fc / fs
b, a = butter(5, w, 'low', fs=fs)

# Mise en forme des données
acceleration_x_array = np.array(acceleration_x)
acceleration_y_array = np.array(acceleration_y)
acceleration_z_array = np.array(acceleration_z)



# Projection des données
proj_x = []
proj_y = []
proj_z = []
for i in range(len(time)):
    rotation = Rotation.from_quat([quaternion_x[i],quaternion_y[i],quaternion_z[i],quaternion_z[i]])
    acceleration_vectors = np.column_stack((acceleration_x_array[i], acceleration_y_array[i], acceleration_z_array[i]))
    rotated_acceleration_vectors = rotation.apply(acceleration_vectors)
    proj_x.append(float(rotated_acceleration_vectors[:, 0]))
    proj_y.append(float(rotated_acceleration_vectors[:, 1]))
    proj_z_tempo = float(rotated_acceleration_vectors[:, 2])
    proj_z.append(proj_z_tempo)

# Calcul de la vitesse
proj_z = np.array(proj_z)+1
velocity_y = -velocity_from_acceleration(time, proj_y)
velocity_x = -velocity_from_acceleration(time, proj_x)
velocity_z = -velocity_from_acceleration(time, proj_z)

# Plotting
#plt.plot(time[:-1], velocity_x, label='Filtered velocity x')
#plt.plot(time[:-1], velocity_z, label='Filtered velocity z')
#plt.plot(time[:-1], velocity_y, label='Filtered velocity y')
plt.plot(time, theoric_velocity_y, label='theoric velocity')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Theoric velocity given by the robot')
plt.legend()
plt.grid(True)
plt.show()
