import time
import csv
from spherov2 import scanner
import spherov2.toy.bolt as boltclass
from spherov2.sphero_edu import SpheroEduAPI
import threading
import spherov2

# Fonction pour avancer le robot en ligne droite


def avancer_en_ligne_droite(droid):
    time.sleep(2)
    droid.roll(0,80,3)
    droid.roll(0,90,3)
    time.sleep(2)

# Fonction pour enregistrer les valeurs de l'accéléromètre pendant que le robot avance


def enregistrer_accelerometre_pendant_avance(droid, duree, fichier_csv):
    with open(fichier_csv, mode='a', newline='') as file:
        writer = csv.writer(file)
        start_time = time.time()
        donnees = {}
        while time.time() - start_time < duree:
            accelerometer_data = droid.get_acceleration()
            orientation_values = droid.get_orientation()
            velocity_values = droid.get_velocity()
            quaternion_values = droid.get_quaternion()
            if accelerometer_data or orientation_values is not None:
                accelerometer_values = [accelerometer_data.get(
                    'x', None), accelerometer_data.get('y', 0), accelerometer_data.get('z', 0)]
                gyro_values = [orientation_values.get('pitch', 0), orientation_values.get(
                    'roll', 0), orientation_values.get('yaw', 0)]
                quaternion_values = [quaternion_values.get('x',0),quaternion_values.get('y',0),quaternion_values.get('z',0),quaternion_values.get('w',0)]
                roll_values = [velocity_values['y']]
                temps_actuel = time.time()-start_time
                data_actuel = gyro_values + accelerometer_values + roll_values + quaternion_values
                if donnees:
                    previous_temps = max(donnees.keys())
                    if donnees[previous_temps] != data_actuel:
                        donnees[temps_actuel] = data_actuel
                        print(data_actuel)
                        writer.writerow([temps_actuel]+data_actuel)
                else:
                    donnees[temps_actuel] = data_actuel
                    writer.writerow([temps_actuel] + data_actuel)
            else:
                print("Aucune donnée d'accélération disponible")


# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot_2)
print(toy)

# Connexion au robot
with SpheroEduAPI(toy) as droid:
    # Nom du fichier CSV pour enregistrer les données de l'accéléromètre
    fichier_csv = "data/donnees_accelerometer_V3_7.csv"

    #spherov2.commands.system_info.DeviceModes(3)
    #scanner.BOLT.set_sensor_streaming_mask(toy, 150, 0, 63431290)
    #scanner.BOLT.set_extended_sensor_streaming_mask(toy, 63431290)
    scanner.BOLT.set_sensor_streaming_mask(toy, 50, 0, 63431290)
    print(scanner.BOLT.get_sensor_streaming_mask(toy))

    thread_data = threading.Thread(target=enregistrer_accelerometre_pendant_avance, args=(droid, 15, fichier_csv))
    # Création du thread pour faire avancer le robot en ligne droite
    #thread_avancer = threading.Thread(target=avancer_en_ligne_droite, args=(droid,))
    #thread_avancer.start()
     # Démarrage du thread pour faire avancer le robot

# Enregistrement des données de l'accéléromètre pendant que le robot avance
    #thread_stabiliser = threading.Thread(target=droid.set_stabilization,args=(True,))
    #enregistrer_accelerometre_pendant_avance(droid,25,fichier_csv)
    thread_data.start()
    avancer_en_ligne_droite(droid)
    # Attendre que le thread d'avancement du robot se termine
    #thread_stabiliser.start()



    #thread_stabiliser.join()
    #thread_avancer.join()
    thread_data.join()


print('thread terminé')
