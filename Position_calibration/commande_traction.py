# Programme permettant de commander le robot en détectant les erreurs de traction et en essayant de les corriger

import time
from spherov2 import scanner
import spherov2.controls.v2
from spherov2.sphero_edu import SpheroEduAPI
import threading
import numpy as np
from scipy.signal import butter, filtfilt
from scipy import integrate as intg


def velocity_from_acceleration(time, acceleration):
    dt = np.diff(time)  # Calculate time intervals
    velocity = intg.cumulative_trapezoid(acceleration, time, initial=0)
    return velocity


def acceleration_progressive(droid, target_speed, initial_speed, time_action):
    delta = list(np.arange(0, time_action, 1))
    for i in delta:
        droid.roll2(int(np.floor(initial_speed + i *
                    (target_speed - initial_speed) / len(delta))), 1)
    print('la vitesse %s est atteinte.' % target_speed)
    return True


def analyse_glissement(donnees_vitesse_acceleration):
    time = list(donnees_vitesse_acceleration.keys())
    vitesse_x = [data['velocity'][0]
                 for data in donnees_vitesse_acceleration.values()]
    vitesse_y = [data['velocity'][1]
                 for data in donnees_vitesse_acceleration.values()]
    accelerometre_x = np.array([data['accelerometer'][0]
                               for data in donnees_vitesse_acceleration.values()])
    accelerometre_y = np.array([data['accelerometer'][1]
                               for data in donnees_vitesse_acceleration.values()])
    accelerometre_z = np.array([data['accelerometer'][2]
                               for data in donnees_vitesse_acceleration.values()])
    pitch = np.array([data['angle'][0]
                     for data in donnees_vitesse_acceleration.values()])
    real_acceleration_y = (np.cos(pitch * np.pi / 180)) * \
        (-((accelerometre_z-1)-accelerometre_y) * 9.8)
    real_acceleration_z = (np.sin(pitch * np.pi / 180)) * \
        abs(accelerometre_z - np.cos(pitch * np.pi / 180) * 0.98)
    real_velocity_y = vitesse_y[0] + \
        velocity_from_acceleration(time, real_acceleration_y)
    print('theoric : '+str(np.mean(vitesse_y)))
    print('real : '+str(np.mean(real_velocity_y)))
    print(abs(np.mean(vitesse_y)-np.mean(real_velocity_y)))
    if abs(np.mean(vitesse_y)-np.mean(real_velocity_y)) > 1.5:
        return True


def detection_glissement(droid):
    start_time = time.time()
    donnees = {}
    donnes_usefull = {}
    while time.time() - start_time < 1:
        accelerometer_data = droid.get_acceleration()
        velocity_data = droid.get_velocity()
        angle = droid.get_orientation()
        if accelerometer_data is not None:
            accelerometer_values = [accelerometer_data.get(
                'x', 0), accelerometer_data.get('y', 0), accelerometer_data.get('z', 0)]
            roll_values = [velocity_data['x'], velocity_data['y']]
            temps_actuel = time.time() - start_time
            data_actuel = {'accelerometer': accelerometer_values, 'velocity': roll_values, 'angle': [
                angle.get('pitch', 0), angle.get('roll', 0), angle.get('yaw', 0)]}
            if donnees:
                previous_temps = max(donnees.keys())
                if donnees[previous_temps] != data_actuel:
                    donnees[temps_actuel] = data_actuel
                    donnes_usefull[temps_actuel] = data_actuel
            else:
                donnees[temps_actuel] = data_actuel
    if analyse_glissement(donnes_usefull):
        print('GLISSEMENT')
        return True
    else:
        return False


def avancer_en_ligne_droite(droid, vitesse, duree_avancee):
    droid.roll(duration=2, speed=0, heading=0)
    droid.roll(duration=duree_avancee, speed=vitesse, heading=0)
    droid.roll(duration=2, speed=0, heading=0)


if __name__ == '__main__':

    # Adresse MAC du robot Sphero Bolt
    nom_robot_1 = "SK-1AF6"
    nom_robot_2 = "SB-E9DA"

    # Recherche du robot Sphero Bolt
    toy_2 = scanner.find_toy(toy_name=nom_robot_2)

    with SpheroEduAPI(toy_2) as droid:

        # spherov2.controls.v2.SensorControl.__update(toy_2)
        target_speed = 20
        initial_speed = 0
        time_action = 15
        thread_avancer = threading.Thread(
            target=acceleration_progressive, args=(droid, target_speed, initial_speed, time_action))
        thread_avancer.start()
