import csv

csv_file_path = 'donnees.csv'
with open('mars04-2024-at-03-21PM_Gyroscope.csv', 'r') as file1, open('mars04-2024-at-03-21PM_Accéléromètre.csv', 'r') as file2, open(csv_file_path, 'w', newline='') as output_csv:
    reader1 = csv.reader(file1)
    next(reader1)  # Skip header row if exists
    reader2 = csv.reader(file2)
    next(reader2)  # Skip header row if exists

    writer = csv.writer(output_csv)
    writer.writerow(
        ["Packet number", "Gyroscope X (deg/s)", "Gyroscope Y (deg/s)", "Gyroscope Z (deg/s)", "Accelerometer X (g)", "Accelerometer Y (g)", "Accelerometer Z (g)"])

    lines = []
    for row1, row2 in zip(reader1, reader2):
        row2 = row2[1:4]
        combined_row = row1 + row2

        writer.writerow(combined_row)
