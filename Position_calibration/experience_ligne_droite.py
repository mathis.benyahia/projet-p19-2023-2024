import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading

# Fonction pour avancer le robot en ligne droite
def avancer_en_ligne_droite(droid, vitesse,duree_avancee):
    droid.set_stabilization(True)
    droid.roll(duration=2,speed=0,heading=0)
    droid.roll(duration=duree_avancee, speed=vitesse, heading=0)
    droid.roll(duration=duree_avancee, speed=vitesse2,heading=0)
    droid.roll(duration=2, speed=0, heading=0)

# Fonction pour enregistrer les valeurs de l'accéléromètre pendant que le robot avance
def enregistrer_accelerometre_pendant_avance(droid, duree, fichier_csv):
    with open(fichier_csv, mode='a', newline='') as file:
        writer = csv.writer(file)
        start_time = time.time()
        while time.time() - start_time < duree:
            accelerometer_data = droid.get_acceleration()
            orientation_values = droid.get_orientation()
            velocity_values = droid.get_velocity()
            if accelerometer_data or orientation_values is not None:
                accelerometer_values = [accelerometer_data.get('x', 0), accelerometer_data.get('y', 0), accelerometer_data.get('z', 0)]
                gyro_values = [orientation_values.get('pitch',0),orientation_values.get('roll',0),orientation_values.get('yaw',0)]
                roll_values = [velocity_values['y']]
                print(accelerometer_values,gyro_values)
                temps_actuel = time.time()-start_time
                donnees = [temps_actuel] + gyro_values + accelerometer_values + roll_values
                writer.writerow(donnees)
            else:
                print("Aucune donnée d'accélération disponible")
            time.sleep(intervalle_releve_accelerometre)

# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot_2)
print(toy)
# definition des variables
vitesse = 80
vitesse2 = 100
duree_avancee = 6
duree_avancee_2 = 4
duree_totale = 20
intervalle_releve_accelerometre = 0.15

# Connexion au robot
with SpheroEduAPI(toy) as droid:
    # Nom du fichier CSV pour enregistrer les données de l'accéléromètre
    fichier_csv = "data/donnees_accelerometre_3.csv"

    # Création du thread pour faire avancer le robot en ligne droite
    thread_avancer = threading.Thread(target=avancer_en_ligne_droite, args=(droid, vitesse, duree_avancee))
    thread_avancer.start()  # Démarrage du thread pour faire avancer le robot

    # Enregistrement des données de l'accéléromètre pendant que le robot avance
    enregistrer_accelerometre_pendant_avance(droid, duree_totale, fichier_csv)

    # Attendre que le thread d'avancement du robot se termine
    thread_avancer.join()

print('thread terminé')