# Premier programme pour essayer de commander le robot en détectant les erreurs de cap
import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math


def surveiller_angle_lacet(droid, duree, angle_objectif):
    while time.time() - start_time < duree:
        droid.roll(duration=1, speed=50, heading=angle_objectif)


# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot)
print(toy)
# definition des variables
vitesse = 80
duree_totale = 20


# Connexion au robot
with SpheroEduAPI(toy) as droid:

    start_time = time.time()
    angle_objectif = 0
    surveiller_angle_lacet(droid, duree_totale, angle_objectif)
    print('threads terminés')
