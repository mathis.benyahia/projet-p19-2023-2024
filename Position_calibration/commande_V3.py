# Programme permettant de commander le robot en détectant les erreurs de cap et en essayant de les corriger
import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math
from spherov2.types import Color


# Fonction qui initialise la boussole du robot
def set_angle(droid):
    droid.set_stabilization(True)
    # droid.reset_aim()
    droid.calibrate_compass()
    droid.set_compass_direction(0)
    droid.set_stabilization(False)
    droid.reset_aim()

# Fonction qui surveille le Cap


def surveiller_angle_lacet(droid, duree, angle_objectif, start_time):

    droid.set_stabilization(False)
    while time.time() - start_time < duree:  # si la simulation n'a pas touché à sa fin
        orientation_values = droid.get_orientation()
        if orientation_values is not None:
            current_yaw = orientation_values.get('yaw', 0)
            print(current_yaw)  # on récupère l'angle
            angle_difference = angle_objectif - current_yaw
            if abs(angle_difference) > 5:  # Si l'erreur est de plus de 5° (seuil choisi)
                print('Ecart Angulaire')
                # Oriente le robot dans la bonne direction
                droid.roll3(heading=0, speed=1, duration=1)
                droid.set_stabilization(False)
            time.sleep(1)  # Attente avant la prochaine vérification


if __name__ == '__main__':
    # Adresse MAC du robot Sphero Bolt
    nom_robot = "SK-1AF6"
    nom_robot_2 = "SB-E9DA"

    # Recherche du robot Sphero Bolt
    toy = scanner.find_toy(toy_name=nom_robot_2)
    print(toy)
    # definition des variables
    vitesse = 80
    duree_avancee = 10
    duree_totale = 30
    intervalle_releve_accelerometre = 0.15

    # Connexion au robot
    with SpheroEduAPI(toy) as droid:

        start_time = time.time()

        set_angle(droid)
        surveiller_angle_lacet(droid, duree_totale, angle_objectif=0)
        print('threads terminés')
