# Programme permettant de surveiller le cap et de requalibrer régulièrement la boussole du robot
# dans le but de se déplacer en milieu avec des aimants

import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math
from spherov2.types import Color

# Fonction permettant d'afficher une flèche


def arrow():
    arrow_points = [(3, 7), (4, 7), (2, 6), (5, 6), (1, 5), (6, 5)]
    for i in range(8):
        arrow_points.append((3, i))
        arrow_points.append((4, i))
    for point in arrow_points:
        x, y = point
        droid.set_matrix_pixel(x, y, color=Color(255, 0, 0))

# Fonction qui initialise la boussole du robot


def set_angle(droid):
    droid.set_stabilization(True)
    # droid.reset_aim()
    droid.calibrate_compass()
    droid.set_compass_direction(0)
    droid.set_stabilization(False)
    droid.reset_aim()

# Fonction pour surveiller l'angle de lacet et réagir en conséquence


def surveiller_angle_lacet(droid, duree):
    global angle_objectif
    droid.set_stabilization(False)
    while time.time() - start_time < duree:
        set_angle(droid)
        arrow()
        for i in range(3):
            orientation_values = droid.get_orientation()
            if orientation_values is not None:
                current_yaw = orientation_values.get('yaw', 0)
                print(current_yaw)
                angle_difference = angle_objectif - current_yaw
                if abs(angle_difference) > 5:
                    print('ecart')
                    droid.roll(heading=0, speed=1, duration=1)
                    droid.set_stabilization(False)
            time.sleep(1)  # Attente avant la prochaine vérification
    droid.set_stabilization(False)


if __name__ == '__main__':
    # Adresse MAC du robot Sphero Bolt
    nom_robot = "SK-1AF6"
    nom_robot_2 = "SB-E9DA"

    # Recherche du robot Sphero Bolt
    toy = scanner.find_toy(toy_name=nom_robot_2)
    print(toy)
    # definition des variables
    vitesse = 80

    duree_avancee = 10
    duree_totale = 30
    intervalle_releve_accelerometre = 0.15

    # Connexion au robot
    with SpheroEduAPI(toy) as droid:
        # Nom du fichier CSV pour enregistrer les données de l'accéléromètre

        start_time = time.time()
        angle_objectif = 0
        # set_angle(droid)
        surveiller_angle_lacet(droid, duree_totale)

        print('threads terminés')
