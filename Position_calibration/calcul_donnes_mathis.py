import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy.signal import butter, filtfilt
from scipy import integrate as intg
from scipy.spatial.transform import Rotation
from numpy.linalg import norm

def velocity_from_acceleration(time, acceleration):
    velocity = intg.cumulative_simpson(y=acceleration,x=time)
    return velocity


# Load acceleration data from CSV file
time = []
acceleration_x = []
acceleration_y = []
acceleration_z = []
rot_x = []
rot_y=[]
rot_z=[]
theoric_velocity = []
quat_x = []
quat_y = []
quat_z = []
quat_w = []

with open('data/donnees_accelerometer_V3_2.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        rot_x.append(float(row[1]))
        rot_y.append(float(row[2]))
        rot_z.append(float(row[3]))
        acceleration_x.append(float(row[4]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity.append(float(row[7]))
        #quat_x.append(float(row[8]))
        #quat_y.append(float(row[9]))
        #quat_z.append(float(row[10]))
        #quat_w.append(float(row[11]))

# filtrage des données (passe bas pour l'accelerometre et passe haut pour le gyroscope)
"""
Args:
- data : Signal à filtrer.
- cutoff : Fréquence de coupure du filtre en Hz.
- fs : Fréquence d'échantillonnage du signal en Hz.
- order : Ordre du filtre, par défaut 5.
"""
fs = 30
fc = 25
w = fc / fs

b, a = butter(5,w, 'low',fs=30)

# mise en forme des données
acceleration_x_array = (np.array(filtfilt(b,a,acceleration_x))-np.mean(acceleration_x[0:10]))*9.8
acceleration_y_array = (np.array(filtfilt(b,a,acceleration_y))-np.mean(acceleration_y[0:10]))*9.8
acceleration_z_array = (np.array(filtfilt(b,a,acceleration_z))-np.mean(acceleration_z[0:10]))*9.8

rot_x = np.array(rot_x)
rot_y = np.array(rot_y)
rot_z = np.array(rot_z)

#quat_x = np.array(quat_x)-np.mean(quat_x[0:10])
#quat_y = np.array(quat_y)-np.mean(quat_y[0:10])
#quat_z = np.array(quat_z)-np.mean(quat_z[0:10])
#quat_w = np.array(quat_w)-np.mean(quat_w[0:10])
#v1
real_acceleration_y = (np.cos(rot_x)) * (acceleration_z_array - acceleration_y_array) * 0.98
real_acceleration_z = (np.sin(rot_x)) * -(acceleration_z_array - np.cos(rot_x) * 0.98)
#eal_acceleration_y = filtfilt(b,a,list(real_acceleration_y))
'''
#v2 en 3D

proj_x = (acceleration_x_array * np.cos(rot_y) * np.cos(rot_z) +
          acceleration_y_array * (np.cos(rot_x) * np.sin(rot_z) + np.sin(rot_x) * np.sin(rot_y) * np.cos(rot_z)) +
          acceleration_z_array * (np.sin(rot_x) * np.sin(rot_z) - np.cos(rot_x) * np.sin(rot_y) * np.cos(rot_z)))

proj_y = (-acceleration_x_array * np.cos(rot_y) * np.sin(rot_z) +
          acceleration_y_array * (np.cos(rot_x) * np.cos(rot_z) - np.sin(rot_x) * np.sin(rot_y) * np.sin(rot_z)) +
          acceleration_z_array * (np.sin(rot_x) * np.cos(rot_z) + np.cos(rot_x) * np.sin(rot_y) * np.sin(rot_z)))

proj_z = (acceleration_x_array * np.sin(rot_y) +
          acceleration_y_array * (-np.sin(rot_x) * np.cos(rot_y)) +
          acceleration_z_array * (np.cos(rot_x) * np.cos(rot_y)))

#v3 en 3D
proj_x = []
proj_y = []
proj_z = []
for i in range(len(time)):
    rotation = Rotation.from_euler('xyz', [rot_x[i], rot_y[i], rot_z[i]])
    acceleration_vectors = np.column_stack((acceleration_x_array[i], acceleration_y_array[i], acceleration_z_array[i]))
    rotated_acceleration_vectors = rotation.apply(acceleration_vectors)
    proj_x.append(float(rotated_acceleration_vectors[:, 0]))
    proj_y.append(float(rotated_acceleration_vectors[:, 1]))
    proj_z_tempo = float(rotated_acceleration_vectors[:, 2])
    proj_z.append(proj_z_tempo)
#v4 en 3D avec les quaternions

proj_x = []
proj_y = []
proj_z = []
for i in range(len(time)):
    rotation = Rotation.from_quat([quat_x[i],quat_y[i],quat_z[i],quat_w[i]])
    acceleration_vectors = np.column_stack((acceleration_x_array[i], acceleration_y_array[i], acceleration_z_array[i]))
    rotated_acceleration_vectors = rotation.apply(acceleration_vectors)
    proj_x.append(float(rotated_acceleration_vectors[:, 0]))
    proj_y.append(float(rotated_acceleration_vectors[:, 1]))
    proj_z.append(float(rotated_acceleration_vectors[:, 2]))
'''
# Calculate velocity
velocity_y = -25/15*velocity_from_acceleration(time, real_acceleration_y)

# Plotting
# plt.plot(time,acceleration_y)
# plt.plot(time[1:],acceleration_filtered)
# plt.plot(time, alpha_filtered, label=' filtered acceleration')
#plt.plot(time[0:100], proj_x[0:100], label=' rot x')
#plt.plot(time[0:100], proj_y[0:100], label=' rot y')
#plt.plot(time[0:100], proj_z[0:100], label=' rot z')
plt.plot(time[1:], velocity_y, label=' calculated velocity')
plt.plot(time, filtfilt(b,a,theoric_velocity), label=' experimental velocity')
# plt.plot(time,speed,label='velocity from wheels')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Velocity vs Time')
plt.legend()
plt.grid(True)
plt.show()
