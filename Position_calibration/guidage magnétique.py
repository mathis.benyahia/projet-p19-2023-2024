import time
from spherov2 import scanner
import spherov2.controls.v2
from spherov2.sphero_edu import SpheroEduAPI
import threading
import numpy as np
from scipy.signal import butter, filtfilt
from scipy import integrate as intg
import time
import csv
from spherov2 import scanner
from spherov2.sphero_edu import SpheroEduAPI
import threading
import math
from spherov2.types import Color

from commande_traction import detection_glissement, acceleration_progressive
from commande_V3 import surveiller_angle_lacet, set_angle


# Adresse MAC du robot Sphero Bolt
nom_robot = "SK-1AF6"
nom_robot_2 = "SB-E9DA"

# Recherche du robot Sphero Bolt
toy = scanner.find_toy(toy_name=nom_robot_2)
print(toy)


with SpheroEduAPI(toy) as droid:
    scanner.BOLT.set_sensor_streaming_mask(toy, 150, 0, 63431290)
    print(scanner.BOLT.get_sensor_streaming_mask(toy))

    for i in range(3):
        droid.set_stabilization(True)
        droid.calibrate_compass()
        droid.set_compass_direction(0)
        time.sleep(0.5)
        droid.reset_aim()
        droid.roll(0,43,2)
        time.sleep(0.5)
    droid.set_stabilization(False)

