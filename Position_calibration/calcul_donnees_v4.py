import numpy as np
import matplotlib.pyplot as plt
import csv
import EKF
import filterpy

time = []
acceleration_x = []
acceleration_y = []
acceleration_z = []
gyro_x = []
gyro_y = []
gyro_z = []
theoric_velocity_x = []
theoric_velocity_y = []
theoric_velocity_z = []
quaternion_x = []
quaternion_y = []
quaternion_z = []
quaternion_w = []

with open('data/donnees_accelerometer_V5.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        gyro_x.append(float(row[1]))
        gyro_y.append(float(row[2]))
        gyro_z.append(float(row[3]))
        acceleration_x.append(float(row[4]))
        acceleration_y.append(float(row[5]))
        acceleration_z.append(float(row[6]))
        theoric_velocity_x.append(float(row[7]))
        theoric_velocity_y.append(float(row[8]))
        theoric_velocity_z.append(float(0))
        quaternion_x.append(float(row[9]))
        quaternion_y.append(float(row[10]))
        quaternion_z.append(float(row[11]))
        quaternion_w.append(float(row[12]))

X = {time[0]:[0,0,0,0,0,0,quaternion_x[0],quaternion_y[0],quaternion_z[0],quaternion_w[0],0,0,0,0,0,0]}
DATA = {time[k]:[gyro_x[k],gyro_y[k],gyro_z[k],acceleration_x[k],acceleration_y[k],acceleration_z[k]] for k in range(len(time))}
for k in range(len(time)-1):
    X[time[k+1]]=EKF.EKF(X[time[k]],DATA[time[k]])

vitesse_y = []
for k in X.keys():
    vitesse_y.append(X[k][2])
# Plotting
plt.plot(time, vitesse_y, label='Filtered velocity')
plt.plot(time, theoric_velocity_y, label='Experimental velocity')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Velocity vs Time')
plt.legend()
plt.grid(True)
plt.show()
