import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy.signal import butter, filtfilt
from scipy import integrate as intg


def velocity_from_acceleration(time, acceleration):
    dt = np.diff(time)  # Calculate time intervals
    velocity = intg.cumulative_trapezoid(acceleration, time, initial=0)
    return velocity


# Load acceleration data from CSV file
time = []
acceleration_y = []
acceleration_z = []
alpha = []
theoric_velocity = []

with open('data/avr.29-2024-at-02-22PM_Accéléromètre.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        time.append(float(row[0]))
        acceleration_y.append(float(row[2]))
        acceleration_z.append(float(row[3]))

with open('data/avr.29-2024-at-02-22PM_Orientation.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        alpha.append(float(row[1]))

with open('data/avr.29-2024-at-02-22PM_Vélocité.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row if exists
    for row in reader:
        theoric_velocity.append(float(row[2]))
# filtrage des données (passe bas pour l'accelerometre et passe haut pour le gyroscope)
"""
Args:
- data : Signal à filtrer.
- cutoff : Fréquence de coupure du filtre en Hz.
- fs : Fréquence d'échantillonnage du signal en Hz.
- order : Ordre du filtre, par défaut 5.
"""
fs = 6.67
fc = 0.235
w = fc / (fs / 2)

b, a = butter(5, w, 'low')
acceleration_z_filtered = filtfilt(b, a, acceleration_z)
acceleration_y_filtered = filtfilt(b, a, acceleration_y)
alpha = filtfilt(b,a,alpha)
# mise en forme des données

acceleration_z_filtered = np.array(acceleration_z_filtered)
acceleration_y_filtered = np.array(acceleration_y_filtered)-acceleration_y_filtered[110]
alpha = np.array(alpha)

real_acceleration_y = (np.cos(alpha*np.pi/180)) * (-(np.array(acceleration_z)-1-np.array(acceleration_y))*98)
real_acceleration_z = (np.sin(alpha*np.pi/180)) * abs(acceleration_z_filtered-np.cos(alpha*np.pi/180)*98)
# real_acceleration = filtfilt(b,a,real_acceleration)
time = np.array(time)


# Calculate velocity

velocity_y = velocity_from_acceleration(time, real_acceleration_y)
velocity_z = 55.5/0.06*velocity_from_acceleration(time, real_acceleration_z)
# velocity = np.sqrt(velocity_z**2+velocity_y**2)
# Plotting
# plt.plot(time,acceleration_y)
# plt.plot(time[1:],acceleration_filtered)
#plt.plot(time, alpha, label=' filtered acceleration')
#plt.plot(time, real_acceleration_y, label=' filtered acceleration z')
plt.plot(time, velocity_y, label=' filtered velocity')
plt.plot(time, filtfilt(b,a,theoric_velocity), label=' experimental velocity')
# plt.plot(time,speed,label='velocity from wheels')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (cm/s)')
plt.title('Velocity vs Time')
plt.legend()
plt.grid(True)
plt.show()
