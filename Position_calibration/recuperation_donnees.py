def enregistrer_accelerometre_pendant_avance(toy, duree, fichier_csv):
    with open(fichier_csv, mode='a', newline='') as file:
        writer = csv.writer(file)
        start_time = time.time()
        donnees = {}
        while time.time() - start_time < duree:
            accelerometer_data = droid.get_acceleration()
            gyro_data = droid.get_gyroscope()
            velocity_values = droid.get_velocity()
            quaternion_values = droid.get_quaternion()
            if accelerometer_data or gyro_data is not None:
                accelerometer_values = [accelerometer_data.get(
                    'x', None), accelerometer_data.get('y', 0), accelerometer_data.get('z', 0)]
                gyro_values = [gyro_data.get('pitch', 0), gyro_data.get(
                    'roll', 0), gyro_values.get('yaw', 0)]
                quaternion_values = [quaternion_values.get('x',0),quaternion_values.get('y',0),quaternion_values.get('z',0),quaternion_values.get('w',0)]
                roll_values = [velocity_values['x'],velocity_values['y'],velocity_values['z']]
                temps_actuel = time.time()-start_time
                data_actuel = gyro_values + accelerometer_values + roll_values + quaternion_values
                if donnees:
                    previous_temps = max(donnees.keys())
                    if donnees[previous_temps] != data_actuel:
                        donnees[temps_actuel] = data_actuel
                        print(data_actuel)
                        writer.writerow([temps_actuel]+data_actuel)
                else:
                    donnees[temps_actuel] = data_actuel
                    writer.writerow([temps_actuel] + data_actuel)
            else:
                print("Aucune donnée d'accélération disponible")


